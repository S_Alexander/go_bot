import numpy as np
from space import Space
import benson
from board import Board

class Game:
    def __init__(self):
        self.board = Board()
        self.color = Space.BLACK
        self.opposite = Space.opposite(self.color)
        self.history = []
        self.positions = []
    
    def get_copy(self):
        copy = Game()
        copy.board = self.board.get_copy()
        copy.color = self.color
        copy.opposite = self.opposite
        copy.history = self.history[:]
        copy.positions = self.positions[:]
        return copy
    
    def set_color(self, color):
        self.color = color
        self.opposite = Space.opposite(self.color)
    
    def switch_colors(self):
        self.color = Space.opposite(self.color)
        self.opposite = Space.opposite(self.color)
    
    def mmove_api(self, xy):
        if xy == (-1, -1):
            self.switch_colors()
            return
        self.board.set_xy_api(xy, self.color)
        op = self.opposite
        for mxy in self.board.adj_api(xy):
            if self.board.test_color(mxy, op):
                stones, _, libs, _ = self.board.get_chain_api(mxy)
                if len(libs) == 0:
                    self.board.remove_stones_api(stones)
        self.switch_colors()
    
    def try_move_api(self, xy):
        if xy == (-1, -1):
            self.switch_colors()
            return True
        if not self.board.test_color(xy, Space.EMPTY):
            return False
        backup = self.board.get_copy()
        self.board.set_xy_api(xy, self.color)
        op = self.opposite
        for mxy in self.board.adj_api(xy):
            if self.board.test_color(mxy, op):
                stones, _, libs, _ = self.board.get_chain_api(mxy)
                if len(libs) == 0:
                    self.board.remove_stones_api(stones)
        stones, _, libs, _ = self.board.get_chain_api(xy)
        if len(libs) == 0:
            self.board = backup
            return False
        hash = self.board.get_hash()
        if xy in self.history:
            if hash in self.positions:
                self.board = backup
                return False
        self.history.append(xy)
        self.positions.append(hash)
        self.switch_colors()
        return True
                
    def to_data(self):
        return self.board.data_six_planes(self.color)
    
    def __str__(self):
        return self.board.__str__()