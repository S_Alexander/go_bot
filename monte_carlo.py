from train import EarlyStopping
from space import Space
import numpy as np

class SmplMCTS:
    def __init__(self, game, net):
        self.game = game
        self.net = net
    
    def move_to_xy(self, move):
        if move == 81:
            x, y = -1, -1
        else:
            y = move // 9
            x = move % 9
        m = (x, y)
        return m
    
    def rollout(self, move):
        roll = self.game.get_copy()
        xy = self.move_to_xy(move)
        #print(xy)
        att = roll.try_move_api(xy)
        #print(att)
        #print(roll)
        if not att:
            return 0
        
        while True:
            data = roll.to_data()
            r = self.net.predict_proba([data])
            r = r.flatten()
            legal = roll.get_legal()
            r_legal = r[legal]
        
            uni = np.multiply(r_legal, r_legal)
            uni = uni / uni.sum()
            ordered = np.random.choice(legal, size=len(legal), replace=False, p=uni)
            ordered = np.append(ordered, 81)
            
            for m in ordered:
                m = self.move_to_xy(m)
                att = roll.try_move_api(m)
                if att:
                    #m = self.move_api_to_gtp(m)
                    break
            #print(roll)
            if m == (-1, -1):
                score = roll.get_benson_score()
                if self.game.color == Space.WHITE and score > 0:
                    return 1
                elif self.game.color == Space.BLACK and score < 0:
                    return 1
                else:
                    return 0
    
    def generate(self, rollouts):
        board = self.game
        data = board.to_data()
        r = self.net.predict_proba([data])
        r = r.flatten()
        
        legal = board.get_legal()
        
        legal = np.append(legal, 81)
        inds = list(range(len(legal)))
        priors = r[legal]
        priors = np.multiply(priors, priors)
        priors = priors / priors.sum()
        N = np.zeros(len(legal), dtype=int)
        W = np.zeros(len(legal), dtype=int)
        P = np.array(priors, copy = True)
        
        while N.sum() < rollouts:
            P = P / P.sum()
            choice = np.random.choice(inds, p=P)
        
            move = legal[choice]
            res = self.rollout(move)
        
            N[choice] += 1
            W[choice] += res
            P[choice] = max(W[choice] / N[choice], priors[choice] / (1 + N[choice]))
        
        order = np.argsort(N)[::-1]
        
        self.log = "Rollouts:\n"
        for ind in order:
            move = legal[ind]
            prior = priors[ind]
            n = N[ind]
            w = W[ind] / N[ind]
            m = self.move_to_xy(move)
            m = self.move_api_to_gtp(m)
            self.log += f"{m}: {prior:.2f}, {n} - {w:.2f}\n"
            
        ind = np.argmax(N)
        return self.move_to_xy(legal[ind])
        
    api_to_char = {0: 'A', 1: 'B', 2: 'C',
                   3: 'D', 4: 'E', 5: 'F',
                   6: 'G', 7: 'H', 8: 'J'}
        
    def move_api_to_gtp(self, m):
        x, y, *_ = m
        if x == -1 or y == -1:
            return 'pass'
        else:
            char = self.api_to_char[x]
            number = (8 - y) + 1
            number = str(number)
            return char + number