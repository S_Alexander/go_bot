from space import Space
import numpy as np
import conversions as convs

class Benson:
    def __init__(self, board):
        self.board = board
        self.bl = self.solve(Space.BLACK)
        self.wh = self.solve(Space.WHITE)
        self.bl_en = self.unlivable_filler(self.bl, Space.BLACK)
        self.wh_en = self.unlivable_filler(self.wh, Space.WHITE)
        
    def fill_borders(self, stones, color):
        size = self.board.get_size()
        opposite = Space.opposite(color)
        construct = np.full((size,size), Space.EMPTY)
        for x,y in stones:
            construct[y,x] = color
        for y in range(size):
            for x in range(size):
                if self.board.test_color((x,y), opposite):
                    construct[y,x] = opposite
        for y in range(size):
            for x in range(size):
                for xo,yo in self.board.adj_api((x,y)):
                    if construct[yo,xo] == color and construct[y,x] == Space.EMPTY:
                        construct[y,x] = opposite
                cuts = 0
                total = 0
                for xo,yo in self.board.diag_api((x,y)):
                    if construct[yo,xo] == color:
                        cuts += 1
                    total += 1
                if cuts >= 2 or (cuts == 1 and total <= 2):
                    if construct[y,x] == Space.EMPTY:
                        construct[y,x] = opposite
        return construct
        
    def unlivable_filler(self, stones, color):
        opposite = Space.opposite(color)
        size = self.board.get_size()
        construct = self.fill_borders(stones, color)###???
        size = self.board.get_size()
        mapping = np.full((size, size), -1, dtype=int)
        for y in range(size):
            for x in range(size):
                if (x,y) in stones:
                    mapping[y, x] = -2
        for y in range(size):
            for x in range(size):
                if mapping[y, x] != -1:
                    continue
                #print(mapping)
                region = [(x,y)]
                i = 0
                while i < len(region):
                    xo, yo = region[i]
                    for xt, yt in self.board.adj_api((xo, yo)):
                        if mapping[yt,xt] == -1 and (xt,yt) not in region:
                            region.append((xt, yt))
                    i += 1
                #print(region)
                count = 0
                for xo,yo in region:
                    if construct[yo,xo] == Space.EMPTY:
                        count += 1
                for xo,yo in region:
                    mapping[yo, xo] = count
        enclosed = []
        #print(mapping)
        mapping = np.logical_and(mapping >= 0, mapping < 2)
        #print(mapping)
        for y in range(size):
            for x in range(size):
                if mapping[y, x]:
                    enclosed.append((x,y))
        return enclosed
        
    def solve(self, color):
        opposite = Space.opposite(color)
        chains = dict()
        libs = dict()
        regions = dict()
        empty = dict()
        size = self.board.get_size()
        mapping = np.zeros((size, size), dtype=int)
        code = 1
        for y in range(size):
            for x in range(size):
                xy = x, y
                if mapping[y, x] != 0:
                    continue
                if self.board.test_color(xy, color):
                    stones, adjs, adj_empts, _ = self.board.get_chain_api(xy)
                    chains[code] = stones
                    libs[code] = adjs.union(adj_empts)
                    for xo,yo in stones:
                        mapping[yo,xo] = code
                    code += 1
                if self.board.test_color(xy, Space.EMPTY):
                    re, em = self.chain_empty(x, y, opposite)
                    #print(re, em)
                    regions[code] = re
                    empty[code] = em
                    for xo,yo in re:
                        mapping[yo,xo] = code
                    code += 1
        #print(mapping)
        #print(chains)
        #print(libs)
        #print(regions)
        #print(empty)
        while True:
            c_to_remove = set()
            r_to_remove = set()
            for c in chains:
                li = libs[c]
                n = 0
                for r in regions:
                    em = empty[r]
                    if em <= li:
                        n += 1
                if n < 2:
                    c_to_remove.add(c)
            for c in c_to_remove:
                li = libs[c]
                for li_in in li:
                    for r in regions:
                        if li_in in regions[r]:
                            r_to_remove.add(r)
            if len(c_to_remove) == 0 and len(r_to_remove) == 0:
                break
            for c in c_to_remove:
                chains.pop(c)
            for r in r_to_remove:
                regions.pop(r)
        result = []
        for c in chains.values():
            result += c
        return result

    def chain_empty(self, x, y, opposite):
        region = [(x,y)]
        empty = {(x,y)}
        i = 0
        while i < len(region):
            xo, yo = region[i]
            for xy in self.board.adj_api((xo, yo)):
                if self.board.test_color(xy, Space.EMPTY) and xy not in region:
                    region.append(xy)
                    empty.add(xy)
                if self.board.test_color(xy, opposite) and xy not in region:
                    region.append(xy)
            i += 1
        return region, empty
    
    def get_settled(self):
        return self.bl_en + self.wh_en
        
    def get_score(self):
        return 5.5 + len(self.wh) + len(self.wh_en) - len(self.bl) - len(self.bl_en)
        #return self.wh, self.wh_en, self.bl, self.bl_en, 5.5 + len(self.wh) + len(self.wh_en) - len(self.bl) - len(self.bl_en)