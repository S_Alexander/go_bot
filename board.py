from space import Space
import conversions as convs
import numpy as np

class Board:
    def __init__(self):
        self.size = size = 9
        self.size_ext = size_ext = self.size + 2
        self.field = np.full((size_ext, size_ext), Space.BORDER)
        self.field[1:-1,1:-1] = Space.EMPTY
    
    def get_size(self):
        return self.size
    
    def get_copy(self):
        copy = Board()
        copy.field = np.array(self.field, copy=True)
        return copy
    
    def load(self, s):
        pass
    
    def get_xy_ext(self, xy):
        x, y = xy
        return self.field[y, x]
    
    def set_xy_ext(self, xy, c):
        x, y = xy
        self.field[y, x] = c
    
    def get_xy_api(self, xy):
        xy = convs.api_to_ext(xy)
        return self.get_xy_ext(xy)
    
    def set_xy_api(self, xy, c):
        xy = convs.api_to_ext(xy)
        self.set_xy_ext(xy, c)

    def adj_api(self, xy):
        x, y = convs.api_to_ext(xy)
        adjs = [(x, y+1), (x, y-1), (x+1, y), (x-1, y)]
        adjs = [xy for xy in adjs if self.get_xy_ext(xy) != Space.BORDER]
        adjs = [convs.ext_to_api(xy) for xy in adjs]
        return adjs
    
    def diag_api(self, xy):
        x, y = convs.api_to_ext(xy)
        adjs = [(x+1, y+1), (x+1, y-1), (x-1, y+1), (x-1, y-1)]
        adjs = [xy for xy in adjs if self.get_xy_ext(xy) != Space.BORDER]
        adjs = [convs.ext_to_api(xy) for xy in adjs]
        return adjs
    
    def get_chain_api(self, xy):
        if self.get_xy_api(xy) == Space.EMPTY:
            return list(), set(), set(), Space.EMPTY
        color = self.get_xy_api(xy)
        opposite = Space.opposite(color)
        stones = [xy]
        adjs = set()
        libs = set()
        i = 0
        while i < len(stones):
            oxy = stones[i]
            for mxy in self.adj_api(oxy):
                if self.get_xy_api(mxy) == color and mxy not in stones:
                    stones.append(mxy)
                if self.get_xy_api(mxy) == opposite:
                    adjs.add(mxy)
                if self.get_xy_api(mxy) == Space.EMPTY:
                    libs.add(mxy)
            i += 1
        return stones, adjs, libs, color
    
    def remove_stones_api(self, stones):
        for xy in stones:
            self.set_xy_api(xy, Space.EMPTY)
    
    def test_color(self, xy, color):
        return self.get_xy_api(xy) == color
    
    def map_groups(self):
        size = self.get_size()
        mapping = np.zeros((size,size), dtype=int)
        groups = dict()
        code = 1
        for y in range(size):
            for x in range(size):
                if mapping[y,x] != 0:
                    continue
                if self.test_color((x,y), Space.EMPTY):
                    continue
                stones, adjs, libs, color = self.get_chain_api((x,y))
                for xo,yo in stones:
                    mapping[yo,xo] = code
                groups[code] = {'stones':stones,
                                'adjs':adjs,
                                'libs':libs,
                                'color' :color}
                code += 1
        return mapping, groups
    
    def liberties_plane(self):
        plane = np.zeros((self.size, self.size), dtype=int)
        for y in range(self.size):
            for x in range(self.size):
                if plane[y, x] != 0:
                    continue
                stones, _, libs, _ = self.get_chain_api((x,y))
                for xo, yo in stones:
                    plane[yo, xo] = len(libs)
        return plane
                
    def data_six_planes(self, color):
        opposite = Space.opposite(color)
    
        plane = self.liberties_plane()
        
        me = self.field[1:-1,1:-1] == color
        me_one = np.logical_and(plane == 1, me).astype(float)
        me_two = np.logical_and(plane == 2, me).astype(float)
        me_many = np.logical_and(plane >= 3, me).astype(float)
        
        op = self.field[1:-1,1:-1] == opposite
        op_one = np.logical_and(plane == 1, op).astype(float)
        op_two = np.logical_and(plane == 2, op).astype(float)
        op_many = np.logical_and(plane >= 3, op).astype(float)
        
        return np.stack((me_one,
                         me_two,
                         me_many,
                         op_one,
                         op_two,
                         op_many))
    
    def get_hash(self):
        symbols = []
        for v in self.field[1:-1,1:-1].flatten():
            symbols.append(str(v))
        return ''.join(symbols)
    
    def __str__(self):
        symbols = []
        s = self.size_ext
        for y in range(s):
            for x in range(s):
                symbol = str(self.field[y,x])
                symbols.append(symbol)
            symbols.append('\n')
        return ''.join(symbols)