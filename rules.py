from space import Space
import numpy as np
import benson
import conversions as convs

def capture_atari(groups, opposite):
    condition = lambda v: v['color'] == opposite and len(v['libs']) == 1
    op_groups = [v for k,v in groups.items() if condition(v)]
    counter = dict()
    for gr in op_groups:
        xy = gr['libs'].pop()
        n = counter.get(xy, 0) + len(gr['stones'])
        counter[xy] = n
    counter = {k:v for k,v in counter.items() if v > 1}
    reasons = {k:f"captures {v}" for k,v in counter.items()}
    return counter, reasons

def check_eye_fill(board, mxy, color):
    adj = board.adj_api(mxy)
    diag = board.diag_api(mxy)
    c_adj = 0
    c_diag = 0
    for xy in adj:
        if board.test_color(xy, color):
            c_adj += 1
    for xy in diag:
        if board.test_color(xy, color):
            c_diag += 1
    if len(adj) + len(diag) <= 5:
        return c_adj + c_diag == len(adj) + len(diag)
    else:
        return (c_adj == len(adj)) and (c_diag >= 3)
    
def check_forbidden(game, mxy, forbidden, reasons):
    board = game.board
    color = game.color
    if not board.test_color(mxy, Space.EMPTY):
        return
    if check_eye_fill(board, mxy, color):
        forbidden.append(mxy)
        reasons[mxy] = "filling eye in"
    
def get_candidates(game):
    candidates = dict()
    reasons = dict()
    color = game.color
    opposite = Space.opposite(color)
    
    mapping, groups = game.board.map_groups()
    
    part_c, part_r = capture_atari(groups, opposite)
    for xy in part_c:
        value = part_c[xy]
        reason = part_r[xy]
        candidates[xy] = candidates.get(xy, 0) + value
        reasons[xy] = reasons.get(xy, []) + [reason]
    
    candidates = sorted(candidates,key=lambda k:candidates[k])[::-1]
    candidates = [convs.api_to_linear(xy) for xy in candidates]
    reasons = {k:', '.join(v) for k,v in reasons.items()}
    
    if len(candidates) == 0:
        candidates = np.array([], dtype=int)
    
    return candidates, reasons
    
def get_forbidden(game):
    ben = benson.Benson(game.board)
    settled = ben.get_settled()
    reasons = {k:"settled" for k in settled}
    return settled, reasons
    
def find_patterns(game):
    candidates, reasons_c = get_candidates(game)
    forbidden, reasons_f = get_forbidden(game)
    reasons_c.update(reasons_f)
    return candidates, forbidden, reasons_c