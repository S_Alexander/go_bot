import os
import sys
import numpy as np
from sklearn.utils import shuffle
import _pickle as pickle
from lasagne import layers
from lasagne.updates import nesterov_momentum
from nolearn.lasagne import NeuralNet
from nolearn.lasagne import BatchIterator
import lasagne
import cv2
import time
import theano.tensor as T
import theano
from scipy import signal
import random

class EarlyStopping(object):
    def __init__(self, patience=100):
        self.patience = patience
        self.best_valid = np.inf
        self.best_valid_epoch = 0
        self.best_weights = None
        self.last = np.inf
        self.impr = 0
        
    def __call__(self, nn, train_history):
        current_valid = train_history[-1]['valid_loss']
        current_epoch = train_history[-1]['epoch']
        if current_valid < self.last:
            self.impr += 1
        self.last = current_valid
        if current_valid < self.best_valid:
            self.best_valid = current_valid
            self.best_valid_epoch = current_epoch
            self.best_weights = nn.get_all_params_values()
        elif self.best_valid_epoch + self.patience < current_epoch:
            print("Early stopping.")
            print("Best valid loss was {:.6f} at epoch {}.".format(
                self.best_valid, self.best_valid_epoch))
            nn.load_params_from(self.best_weights)
            print("Percent improvement: {:.2f}".format(self.impr/current_epoch))
            raise StopIteration()
        if current_epoch % 100 == 0:
            print("Percent improvement: {:.2f}".format(self.impr/current_epoch))

            
RATE = 0.03 #0.005, highest 0.03
MOMENTUM = 0.5
PATIENCE = 100
EPOCHS = 1000

net = NeuralNet(
    layers=[('input', lasagne.layers.InputLayer),
            ('conv', lasagne.layers.Conv2DLayer),
            ('hidden', lasagne.layers.DenseLayer),
            ('output', lasagne.layers.DenseLayer),],
    input_shape=(None, 6, 9, 9),
    conv_num_filters=12, 
    conv_filter_size=(5, 5), 
    hidden_num_units=100,
    output_num_units=82, 
    output_nonlinearity=lasagne.nonlinearities.softmax,
    update=nesterov_momentum,
    update_learning_rate=RATE,
    update_momentum=MOMENTUM,
    on_epoch_finished=[EarlyStopping(patience=PATIENCE),],
    regression=False,
    max_epochs=EPOCHS,
    verbose=1,
    batch_iterator_train=BatchIterator(batch_size=16384),)

def test(net):
    print("Testing")
    data = pickle.load(open("data_test.p", "rb"))
    X = data['X']
    Y = data['Y']
    X, Y = shuffle(X, Y, random_state=42)
    Y = Y.flatten()
    r = net.predict_proba(X)
    correct = 0
    positive = 0
    negative = 0
    table = np.zeros((2, 17))
    for i in range(len(r)):
        ms = X[i,:-1].astype(int).sum() // 5
        if np.argmax(r[i]) == Y[i]:
            correct += 1
            table[0][ms] += 1
        table[1][ms] += 1
        p = r[i][Y[i]]
        n = r[i].sum() - p
        positive += p
        negative += n
    acc = 100 * correct / len(r)
    print(f"Accuracy: {acc:.2f}%")
    av_pos = 100 * positive / len(r)
    print(f"Average positive: {av_pos:.2f}%")
    av_neg = 100 * negative / len(r) / 81
    print(f"Average negative: {av_neg:.2f}%")
    print("By moves:")
    for i in range(len(table[0])):
        minv = i * 5
        maxv = i * 5 + 4
        acc = 100 * table[0][i] / table[1][i]
        print(f"{minv}-{maxv}: {acc:.2f}%")
    
    
def main():
    data = pickle.load(open("data.p", "rb"))
    X = data['X']
    Y = data['Y']
    X, Y = shuffle(X, Y, random_state=42)
    Y = Y.flatten()
    
    try:
        with open('net.p', 'rb') as f:
            saved = pickle.load(f)
            saved_layers = saved.get_all_layers()
            net.initialize()
            layers = net.get_all_layers()
            params = lasagne.layers.get_all_param_values(saved_layers)
            lasagne.layers.set_all_param_values(layers, params) 
    except (OSError, IOError) as e:
        net.initialize()
    
    while True:
        test(net)
        net.train_loop(X, Y, 100)
        with open('net.p', 'wb') as f:
            pickle.dump(net, f, -1)
       

if __name__ == "__main__":
    main()