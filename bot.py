import os
import sys
import numpy as np
from sklearn.utils import shuffle
import _pickle as pickle
import time
import random
from train import EarlyStopping
from game import Game
from space import Space
import logging
import rules
import conversions as convs

class RamblingBot:
    
    def __init__(self, net):
        self.game = Game()
        self.net = net
        self.stop = False
        pid = str(os.getpid())
        startup = str(int(time.time()))
        name = pid + '-' + startup
        filename = 'C:/123/data/programs/go_bot/logs/'+name+".log"
        logging.basicConfig(filename=filename, level=logging.INFO)
        
        self.commands = {'name'            :self.get_name,
                         'version'         :self.get_version,
                         'protocol_version':self.get_protocol_v,
                         'list_commands'   :self.get_commands,
                         'komi'            :self.komi_command,
                         'boardsize'       :self.size_command,
                         'clear_board'     :self.clear_board,
                         'play'            :self.play_command,
                         'genmove'         :self.genmove,
                         'showboard'       :self.showboard,
                         'quit'            :self.quit,}
    
    def get_name(self, args=None):
        return "= Rambling Bot"
    
    def get_version(self, args=None):
        return "= 0.2dev"
    
    def get_protocol_v(self, args=None):
        return "= 2"
    
    def get_commands(self, args=None):
        return "= " + '\n'.join(self.commands.keys())
        
    def komi_command(self, args):
        return "= "
    
    def size_command(self, args):
        size = args[0]
        if size == '9':
            return "= "
        else:
            return "? unacceptable size"
    
    def clear_board(self, args=None):
        self.game = Game()
        return "= "
    
    def play_command(self, args):
        color = args[0]
        color = convs.to_color[color]
        self.game.set_color(color)
        
        place = args[1]
        mxy = convs.gtp_to_api(place)
        self.game.mmove_api(mxy)
        return "= "
    
    def genmove(self, args):
        color = args[0]
        color = convs.to_color[color]
        self.game.set_color(color)
        
        candidates, forbidden, reasons = rules.find_patterns(self.game)
        logging.info("Candidates: " + str(candidates))
        logging.info("Forbidden: " + str(forbidden))

        data = self.game.to_data()
        r = self.net.predict_proba([data])
        r = r.flatten()[:-1]
        moves = list(range(len(r)))
        
        uni = np.multiply(r, r)
        uni = uni / uni.sum()
        ordered = np.random.choice(moves, size=len(moves), replace=False, p=uni)
        
        total = np.concatenate((candidates, ordered, [81]))
        #print(total)
        illegals = set()
        for offset in total:
            mxy = convs.linear_to_api(offset)
            m = convs.api_to_gtp(mxy)
            if mxy not in forbidden:
                rules.check_forbidden(self.game, mxy, forbidden, reasons)
            if mxy in forbidden:
                logging.info("Forbidden: " + m + f" ({reasons[mxy]})")
                continue
            if self.game.try_move_api(mxy):
                if len(illegals) != 0:
                    logging.info("Illegals: " + ", ".join(illegals))
                reason = reasons.get(mxy, "net")
                logging.info("Move: " + m + f" ({reason})")
                sys.stderr.write(self.game.__str__())
                sys.stderr.flush()
                return "= " + m
            else:
                illegals.add(m)
                
    
    def showboard(self, args=None):
        return "= "
    
    def quit(self, args=None):
        self.stop = True
        return "= "
    
    def unknown(self, args=None):
        return "? unknown command"
    
    def loop(self):
        self.stop = False
        for line in sys.stdin:
            line = line.strip('\n')
            logging.info("")
            logging.info(line)
            command, *args = line.split(' ')
            handler = self.commands.get(command, self.unknown)
            response = handler(args)
            logging.info(response)
            sys.stdout.write(response + '\n\n')
            sys.stdout.flush()
            if self.stop:
                return

def get_bot():
    net = pickle.load(open("C:/123/data/programs/go_bot/net.p", "rb"))
    bot = RamblingBot(net)
    return bot
                
def main():
    bot = get_bot()
    bot.loop()

if __name__ == "__main__":
    main()