import _pickle as pickle
import lasagne
from train import EarlyStopping

def main():
    net = pickle.load(open('net.p', 'rb'))
    layers = net.get_all_layers()
    params = lasagne.layers.get_all_param_values(layers)
    with open('params.p', 'wb') as f:
        pickle.dump(params, f, -1)

if __name__ == '__main__':
    main()