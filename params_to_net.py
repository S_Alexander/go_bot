import _pickle as pickle
import lasagne
from train import EarlyStopping
from train import net

def main():
    params = pickle.load(open('params.p', 'rb'))
    net.initialize()
    layers = net.get_all_layers()
    lasagne.layers.set_all_param_values(layers, params)
    with open('net.p', 'wb') as f:
        pickle.dump(net, f, -1)

if __name__ == '__main__':
    main()