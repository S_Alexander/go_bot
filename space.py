from enum import Enum


class Space(Enum):
    EMPTY = 0
    BORDER = 1
    BLACK = 2
    WHITE = 3
    
    @classmethod
    def opposite(cls, color):
        if color is cls.BLACK:
            return cls.WHITE
        elif color is cls.WHITE:
            return cls.BLACK
        else:
            return color
            
    def __str__(self):
        if self is Space.EMPTY:
            return '_'
        elif self is Space.BORDER:
            return '='
        elif self is Space.BLACK:
            return '1'
        elif self is Space.WHITE:
            return '2'