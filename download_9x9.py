import _pickle as pickle
import requests
import numpy as np
import json
import matplotlib.pyplot as plt
import time
from io import BytesIO
import math
import random

class Connection(requests.auth.AuthBase):

    def __init__(self, filename, interval=15):
        super().__init__()
        self.interval = interval
        with open(filename) as f:
            data = json.load(f)
            url = 'https://online-go.com/oauth2/token/'
            r = requests.post(url, data = data)
            if r.status_code != 200:
                message = 'Auth failed, response code: {}'.format(r.status_code)
                raise RuntimeError(message)
            self.tokens = json.loads(r.content)
            self.check_auth()
        
    def __call__(self, r):
        full_token = self.tokens['token_type'] + " " + self.tokens['access_token']
        r.headers['Authorization'] = full_token
        return r

    def request_json(self, url, params=None):
        return json.loads(self.request_content(url, params))
        
    def request_content(self, url, params=None):
        time.sleep(self.interval)
        try:
            r = requests.get(url, auth=self, params=params)
        except requests.exceptions.ConnectionError as e:
            print("Connection issues...")
            time.sleep(60)
            r = requests.get(url, auth=self, params=params)
        if r.status_code not in [200, 404, 403]:
            message = 'Bad response code: {} ({})'.format(r.status_code, url)
            raise RuntimeError(message)
        return r.content

    def check_auth(self):
        r = requests.get('https://online-go.com/api/v1/me', auth=self)
        if r.status_code != 200:
            raise RuntimeError('check_auth failed')

def to_numpy(text):
    usecols = [0, 1, 2, 3, 4, 5, 7, 8, 9, 10, 11]
    dtype = [('time', int),
             ('id', int),
             ('black', int), 
             ('hand', int), 
             ('rat', float), 
             ('dev', float), 
             ('op_id', int),
             ('op_r', float), 
             ('op_d', float), 
             ('result', int),
             ('extra', 'S20')]
    data = np.genfromtxt(BytesIO(text), 
                         delimiter='\t', 
                         usecols = usecols, 
                         dtype=dtype, 
                         skip_header=1)
    return data

def to_numpy_str(d):
    return {'all':to_numpy(d['all']), '9':to_numpy(d['9'])}
    
def to_rank(rating):
    return np.log(rating / 850) / 0.032

def correct_game(pl, id):
    v_9 = pl['9']
    v_all = pl['all']
    if id == 0:
        return False
    if id not in v_all['id']:
        return False
    i = np.where(v_all['id'] == id)[0][0]
    row_all = v_all[i]
    rat_me = row_all['rat']
    dev_me = row_all['dev']
    rat_op = row_all['op_r']
    dev_op = row_all['op_d']
    if rat_me < 1600 or rat_op < 1600:
        return False
    if dev_me > 100 or dev_op > 100:
        return False
    return True
    
def get_single(conn, id):
    url = "https://online-go.com/termination-api/player/{}/rating-history"
    url = url.format(id)
    params = {
        'size': 9,
    }
    history_9 = conn.request_content(url, params=params)
    history = conn.request_content(url)
    data = {'all':history, '9':history_9}
    data = to_numpy_str(data)
    print("Downloaded player {}".format(id))
    return data
    
def get_players(conn, players):
    active = dict()
    for pid in players:
        pl = players[pid]
        v_9 = pl['9']
        v_all = pl['all']
        for row_9 in v_9:
            id = row_9['id']
            if not correct_game(pl, id):
                continue
            op_id = row_9['op_id']
            if op_id in players:
                continue
            data = get_single(conn, op_id)
            active[op_id] = data
            #players[op_id] = data
            if len(active) >= 1:
                break
        if len(active) >= 1:
            conn.check_auth()
            break
    if len(active) == 0:
        exit()
    players.update(active)

def get_games(conn, games, active):
    counter = 0
    for pid in active:
        pl = active[pid]
        v_9 = pl['9']
        v_all = pl['all']
        for row_9 in v_9:
            id = row_9['id']
            if not correct_game(pl, id):
                continue
            if id in games:
                continue
            url = "https://online-go.com/api/v1/games/{}"
            url = url.format(id)
            game = conn.request_json(url)
            games[id] = game
            print("Downloaded game {}".format(id))
            counter += 1
            if counter % 100 == 0:
                with open(f"games.p", 'wb') as f:
                    pickle.dump(games, f, -1)
                print("Checkpoint!")
    
    
def go(players, games):
    banned_id = {434218, 
                 484523, 
                 520052, 
                 165325, 
                 433376, 
                 462601, 
                 165325, 
                 58542, 
                 540077,
                 538018,
                 533729}
    conn = Connection('credentials', interval = 30)
    while True:
        if len(players) == 0:
            players = {575693: get_single(conn, 575693)}
        else:
            get_players(conn, players)
        get_games(conn, games, players)
        with open(f"players.p", 'wb') as f:
            pickle.dump(players, f, -1)
        with open(f"games.p", 'wb') as f:
            pickle.dump(games, f, -1)
        print("Saved!")
            
def main():
    try:
        players = pickle.load(open("players.p", "rb"))
        games = pickle.load(open("games.p", "rb"))
    except (OSError, IOError) as e:
        players = dict()
        games = dict()
    go(players, games)

if __name__ == "__main__":
    main()