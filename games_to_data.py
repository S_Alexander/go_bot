import _pickle as pickle
import numpy as np
from board import Board
from sklearn.utils import shuffle

def process(X, Y, game):
    if game['handicap'] != 0:
        return
    b = Board()
    for m in game['gamedata']['moves']:
        x = b.to_data()
        y = b.move_to_code(m)
        X.append(x)
        Y.append(y)
        b.mmove_api(m)

def main():
    games = pickle.load(open("games.p", "rb"))
    X = []
    Y = []
    for g in games.values():
        process(X, Y, g)
    X = np.stack(X)
    Y = np.array(Y)
    X, Y = shuffle(X, Y, random_state=42)
    length = len(X)
    train_length = int(length * 0.8)
    X_train = X[:train_length]
    Y_train = Y[:train_length]
    X_test = X[train_length:]
    Y_test = Y[train_length:]
    data_train = {'X': X_train, 'Y': Y_train}
    data_test = {'X': X_test, 'Y': Y_test}
    with open(f"data.p", 'wb') as f:
        pickle.dump(data_train, f, -1)
    with open(f"data_test.p", 'wb') as f:
        pickle.dump(data_test, f, -1)

if __name__ == "__main__":
    main()