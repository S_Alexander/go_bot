from space import Space

api_to_char = {0:'A', 1:'B', 2:'C',
               3:'D', 4:'E', 5:'F',
               6:'G', 7:'H', 8:'J',}
               
char_to_api = {'A':0, 'B':1, 'C':2,
               'D':3, 'E':4, 'F':5,
               'G':6, 'H':7, 'J':8,}

to_color = {'B'    :Space.BLACK,
            'black':Space.BLACK,
            'W'    :Space.WHITE,
            'white':Space.WHITE,}

def gtp_to_api(m):
    if m == 'pass':
        return -1, -1
    else:
        char = m[0].upper()
        number = m[1:]
        number = int(number)
        x = char_to_api[char]
        y = 8 - (number - 1)
        return x, y

def api_to_gtp(mxy):
    if mxy == (-1, -1):
        return 'pass'
    else:
        x, y, *_ = mxy
        char = api_to_char[x]
        number = (8 - y) + 1
        number = str(number)
        return char + number

def api_to_ext(mxy):
    x, y, *_ = mxy
    return x+1, y+1

def ext_to_api(mxy):
    x, y, *_ = mxy
    return x-1, y-1
    
def linear_to_api(offset):
    if offset == 81:
        return -1, -1
    else:
        y = offset // 9
        x = offset % 9
        return x, y
        
def api_to_linear(mxy):
    x, y = mxy
    return y * 9 + x